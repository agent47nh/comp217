#include<stdio.h>
#include<stdlib.h>
#define SIZE 5
int data[SIZE];
int top=-1;
int isfull()
{
	if(top==SIZE-1)
		return 1;
	else
		return 0;
}
int isempty()
{
	if(top==-1)
		return 1;
	else
		return 0;
}
void push(int x)
{
	if(!isfull())
	{
		top++;
		data[top]=x;
	}
	else
		printf("The stack is full!\n\n");
}
int pop()
{
	int datatemp;
	if(!isempty())
	{
		datatemp=data[top];
		top--;
		return datatemp;
	}
	else
		printf("The stack is empty!");
		return 0;
}
void list()
{
	int i;
	for(i=0;i<=top;i++)
        printf("%d   ",data[i]);
}
int main()
{
	int temp,data;
	while(temp!=4){
		printf("Press 1 to make a push.\nPress 2 to pop.\nPress 3 to list.\nPress 4 to exit\n");
		scanf("%d",&temp);
		switch(temp){
			case 1:
				printf("Please make a data input: ");
				scanf("%d",&data);
				push(data);
				break;
			case 2:
				if(isempty()){
					printf("The stack is empty!\n");
					break;
				}
				printf("The popped data is %d\n", pop());
				break;
			case 3:
				if(isempty()){
					printf("The stack is empty!\n");
					break;
				}
				printf("The list is: \n");
                list();
                printf("\n");
				break;
			case 4:
				printf("Exiting....\n");
				break;
			default:
				printf("Invalid input! Try again....\n");
		}
	}
}
